# DS2019_30642_Mitisor_Maria_Assignment_4
Run the service application first.    
Then, run medical-app. Click "View activities for patients" and a table with the activities will show.    
The activity retrieval functionality has been built using a soap service.    
The request, response and Activity classes have been created with the jaxb plugin. To re-create them, use maven generate sources.     