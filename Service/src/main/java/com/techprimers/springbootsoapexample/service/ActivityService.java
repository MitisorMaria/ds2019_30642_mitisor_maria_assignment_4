package com.techprimers.springbootsoapexample.service;

import com.techprimers.spring_boot_soap_example.Activity;
import com.techprimers.spring_boot_soap_example.LISTTYPE;
import com.techprimers.springbootsoapexample.repository.ActivityRepository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
public class ActivityService {

    private static final Map<Integer, Activity> activities = new HashMap<>();

    private ActivityRepository activityRepository = new ActivityRepository();

    @PostConstruct
    public void initialize() {

        Activity act = new Activity();
        act.setId(100);
        act.setName("Peter");
        act.setEndTime("2010-09-09");
        act.setStartTime("2010-09-07");
        act.setPatientId("2");

        activities.put(Integer.valueOf(act.getId()), act);
    }


    public LISTTYPE getActivities() {
        LISTTYPE list = new LISTTYPE();
        ArrayList<Activity> acts = activityRepository.getActivities();
        list.getData().addAll(acts);
        return list;
    }
}
