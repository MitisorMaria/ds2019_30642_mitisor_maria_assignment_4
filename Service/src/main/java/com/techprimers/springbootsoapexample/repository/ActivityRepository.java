package com.techprimers.springbootsoapexample.repository;

//STEP 1. Import required packages
import com.techprimers.spring_boot_soap_example.Activity;

import java.sql.*;
import java.util.ArrayList;

public class ActivityRepository {
        // JDBC driver name and database URL
        static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        static final String DB_URL = "jdbc:mysql://localhost/medicalapp";

        //  Database credentials
        static final String USER = "root";
        static final String PASS = "paroladelamysql";

        public ArrayList<Activity> getActivities() {
            Connection conn = null;
            Statement stmt = null;
            try{
                //STEP 2: Register JDBC driver
                Class.forName("com.mysql.jdbc.Driver");

                //STEP 3: Open a connection
                System.out.println("Connecting to database...");
                conn = DriverManager.getConnection(DB_URL,USER,PASS);

                //STEP 4: Execute a query
                System.out.println("Creating statement...");
                stmt = conn.createStatement();
                String sql;
                sql = "SELECT id, activity, end_time, patient_id, start_time FROM messages";
                ResultSet rs = stmt.executeQuery(sql);
                ArrayList<Activity> activities = new ArrayList<>();

                //STEP 5: Extract data from result set
                while(rs.next()){
                    //Retrieve by column name
                    int id  = rs.getInt("id");
                    String activity = rs.getString("activity");
                    String end_time = rs.getString("end_time");
                    String patient_id = rs.getString("patient_id");
                    String start_time = rs.getString("start_time");
                    Activity act = new Activity();
                    act.setPatientId(patient_id);
                    act.setStartTime(start_time);
                    act.setEndTime(end_time);
                    act.setName(activity);
                    act.setId(id);
                    activities.add(act);

                }
                //STEP 6: Clean-up environment
                rs.close();
                stmt.close();
                conn.close();
                return activities;
            }catch(SQLException se){
                //Handle errors for JDBC
                se.printStackTrace();
            }catch(Exception e){
                //Handle errors for Class.forName
                e.printStackTrace();
            }finally{
                //finally block used to close resources
                try{
                    if(stmt!=null)
                        stmt.close();
                }catch(SQLException se2){
                }// nothing we can do
                try{
                    if(conn!=null)
                        conn.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }//end try
            System.out.println("Goodbye!");
            return null;
        }//end main

    public Activity getActivity(int id) {
        Connection conn = null;
        Statement stmt = null;
        try{
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT activity, end_time, patient_id, start_time FROM messages WHERE id=" + id;
            ResultSet rs = stmt.executeQuery(sql);

            Activity act = new Activity();
            //STEP 5: Extract data from result set
            while(rs.next()){
                //Retrieve by column name
                String activity = rs.getString("activity");
                String end_time = rs.getString("end_time");
                String patient_id = rs.getString("patient_id");
                String start_time = rs.getString("start_time");

                act.setPatientId(patient_id);
                act.setStartTime(start_time);
                act.setEndTime(end_time);
                act.setName(activity);
                act.setId(id);

            }
            //STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
            return act;
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
        return null;
    }//end main
    }//end FirstExample

