package com.techprimers.springbootsoapexample.endpoint;

import com.techprimers.spring_boot_soap_example.GetActivityRequest;
import com.techprimers.spring_boot_soap_example.GetActivityResponse;
import com.techprimers.springbootsoapexample.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class ActivityEndpoint {

    @Autowired
    private ActivityService activityService;


    @PayloadRoot(namespace = "http://techprimers.com/spring-boot-soap-example",
            localPart = "getActivityRequest")
    @ResponsePayload
    public GetActivityResponse getActivityRequest(@RequestPayload GetActivityRequest request) {
        GetActivityResponse response = new GetActivityResponse();
        response.setActivity(activityService.getActivities());
        return response;
    }
}
