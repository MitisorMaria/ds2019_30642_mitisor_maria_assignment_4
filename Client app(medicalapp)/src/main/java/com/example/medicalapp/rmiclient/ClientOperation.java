package com.example.medicalapp.rmiclient;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import com.example.medicalapp.rmiclient.gui.ClientView;
import com.example.medicalapp.rmiinterface.MedicationInterface;
import com.example.medicalapp.rmiinterface.RMIInterface;

import javax.swing.*;

public class ClientOperation {
	private static RMIInterface look_up;

	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		
		look_up = (RMIInterface) Naming.lookup("//localhost/MyServer");
		ClientView cv = new ClientView();
		while(true){
			String txt = JOptionPane.showInputDialog("Which patient's medication plan do you want to see?");
			ArrayList<MedicationInterface> plan = look_up.getMedPlan(Integer.parseInt(txt));
			cv.clear();
			for (MedicationInterface mi : plan) {
				cv.addMedPlanToView(mi.toString());
			}
		}


	}
}
