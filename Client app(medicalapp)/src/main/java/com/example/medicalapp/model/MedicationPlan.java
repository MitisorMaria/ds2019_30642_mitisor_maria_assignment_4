package com.example.medicalapp.model;

import com.example.medicalapp.rmiinterface.MedicationInterface;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "medicationPlans")
public class MedicationPlan implements Serializable, MedicationInterface {
	
	@Id
	private int id;
	@Column(name = "patientId")
	private int patientId;
	@Column(name = "name")
    private String medName;
	@Column(name = "dosage")
    private int dosage;
	@Column(name = "time")
    private String timeToTake;



    public String getMedName() {
        return medName;
    }

    public void setMedName(String medName) {
        this.medName = medName;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public String getTimeToTake() {
        return timeToTake;
    }

    public void setTimeToTake(String timeToTake) {
        this.timeToTake = timeToTake;
    }
    
    public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}


    public MedicationPlan() {
    }


    public MedicationPlan(int id, int patId, String medName, int dosage, String timesToTake) {
    	this.patientId = patId;
    	this.id = id;
        this.medName = medName;
        this.dosage = dosage;
        this.timeToTake = timesToTake;
    }

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String toString(){
        return "Medication: " + medName + " dosage: " + dosage + " take at: " + timeToTake;
    }


}
