package com.example.medicalapp.model;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "caregivers")
public class Caregiver implements Serializable{
	@Id
	private String id;
	@Column(name = "name")
	private String name;
	@Column(name = "birth_date")
	private String birthDate;
	@Column(name = "gender")
	private String gender;
	@Column(name = "address")
	private String address;
	@Transient
	@OneToMany(mappedBy = "caregiverId")
	private ArrayList<Patient> listOfPatients;
	public Caregiver() {
		
	}
	
	
	public Caregiver(String name, String id, String birthDate, String gender, String address, String medicalRecord) {
		super();
		this.id = id;
		this.birthDate = birthDate;
		this.gender = gender;
		this.address = address;
		this.name = name;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}


}
