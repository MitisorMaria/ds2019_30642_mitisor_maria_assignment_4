package com.example.medicalapp.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "messages")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id", scope = Activity.class)
public class Activity{

	@Id
	private int id;
	@Column(name="patient_id")
	private String patientId;
	@Column(name="start_time")
	private String startTime;
	@Column(name="end_time")
	private String endTime;
	@Column(name="activity")
	private String activity;
	
	public Activity(String patientId, String startTime, String endTime, String activity) {
		super();
		this.patientId = patientId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}
	
	public Activity() {
		
	}
	
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	@Override
	public String toString() {
		return "Message [patientId=" + patientId + ", startTime=" + startTime + ", endTime=" + endTime + 
				", activity=" + activity +"]";
	}
}
