package com.example.medicalapp.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/")
public class GlobalController {
	@RequestMapping("/")
	public String viewHomePage(Model model) {
	    return "index";
	}

}
