package com.example.medicalapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.medicalapp.model.Patient;
import com.example.medicalapp.service.PatientService;

@Controller
@RequestMapping("/patients")
public class PatientController {
	@Autowired
    PatientService patientService;
	
	// Get All Patients
	@GetMapping
	public List<Patient> getAllPatients() {
	    return patientService.findAll();
	}

    // Create a new Patient
	@GetMapping("/new")
	public String newPatient(Model model) {
	    Patient patient = new Patient();
	    model.addAttribute("patient", patient);
	     
	    return "createPatient";
	}

	// Create a new Patient
		@PostMapping(value = "/save",
				consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, 
		        produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
		public String createPatient(@Valid Patient patient, Model model) throws Exception {
		    patientService.update(patient);
		    model.addAttribute("patient", patient);
			return "savedPatient";
		}
	
    // Get a Single Patient
	@GetMapping("/{id}")
	public Patient getPatientById(@PathVariable(value = "id") String patientId) throws Exception {
	    return patientService.getPatientById(patientId);
	}
	
	// edit 
	@RequestMapping("/edit/{id}")
	public String showEditPatientPage(@PathVariable(name = "id") String id, Model model) {
	    Patient patient = patientService.getPatientById(id);
	    model.addAttribute("patient", patient);
	     
	    return "createPatient";
	}

    // Delete a Patient
	@GetMapping("/delete/{id}")
	public String deletePatient(Model model, @PathVariable(value = "id") String patientId) throws Exception {
		Patient patient = patientService.getPatientById(patientId);

	    patientService.delete(patient);

	    List<Patient> listPatients = patientService.findAll();
	    model.addAttribute("listPatients", listPatients);
	     
	    return "indexPatient";
	}
	
	@RequestMapping("/get")
	public String viewHomePage(Model model) {
	    List<Patient> listPatients = patientService.findAll();
	    model.addAttribute("listPatients", listPatients);
	     
	    return "indexPatient";
	}
	
}
