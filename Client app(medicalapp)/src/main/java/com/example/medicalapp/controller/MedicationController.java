package com.example.medicalapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.medicalapp.model.Medication;
import com.example.medicalapp.service.MedicationService;

@Controller
@RequestMapping("/medications")
public class MedicationController {
	@Autowired
    MedicationService medicationService;
	
	// Get All Medications
		@GetMapping
		public List<Medication> getAllMedications() {
		    return medicationService.findAll();
		}

	    // Create a new Medication
		@GetMapping("/new")
		public String newMedication(Model model) {
		    Medication medication = new Medication();
		    model.addAttribute("medication", medication);
		     
		    return "createMedication";
		}

		// Create a new Medication
			@PostMapping(value = "/save",
					consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, 
			        produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
			public String createMedication(@Valid Medication medication, Model model) throws Exception {
			    medicationService.update(medication);
			    model.addAttribute("medication", medication);
				return "savedMedication";
			}
		
	    // Get a Single Medication
		@GetMapping("/{id}")
		public Medication getMedicationById(@PathVariable(value = "id") String medicationId) throws Exception {
		    return medicationService.getMedicationById(medicationId);
		}
		
		// edit 
		@RequestMapping("/edit/{id}")
		public String showEditMedicationPage(@PathVariable(name = "id") String id, Model model) {
		    Medication medication = medicationService.getMedicationById(id);
		    model.addAttribute("medication", medication);
		     
		    return "createMedication";
		}

	    // Delete a Medication
		@GetMapping("/delete/{id}")
		public String deleteMedication(Model model, @PathVariable(value = "id") String medicationId) throws Exception {
			Medication medication = medicationService.getMedicationById(medicationId);

		    medicationService.delete(medication);

		    List<Medication> listMedications = medicationService.findAll();
		    model.addAttribute("listMedications", listMedications);
		     
		    return "indexMedication";
		}
		
		@RequestMapping("/get")
		public String viewHomePage(Model model) {
		    List<Medication> listMedications = medicationService.findAll();
		    model.addAttribute("listMedications", listMedications);
		     
		    return "indexMedication";
		}

}
