package com.example.medicalapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.medicalapp.model.Caregiver;
import com.example.medicalapp.service.CaregiverService;

@Controller
@RequestMapping("/caregivers")
public class CaregiverController {
	@Autowired
    CaregiverService caregiverService;
	
	// Get All Caregivers
		@GetMapping
		public List<Caregiver> getAllCaregivers() {
		    return caregiverService.findAll();
		}

	    // Create a new Caregiver
		@GetMapping("/new")
		public String newCaregiver(Model model) {
		    Caregiver caregiver = new Caregiver();
		    model.addAttribute("caregiver", caregiver);
		     
		    return "createCaregiver";
		}

		// Create a new Caregiver
			@PostMapping(value = "/save",
					consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, 
			        produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
			public String createCaregiver(@Valid Caregiver caregiver, Model model) throws Exception {
			    caregiverService.update(caregiver);
			    model.addAttribute("caregiver", caregiver);
				return "savedCaregiver";
			}
		
	    // Get a Single Caregiver
		@GetMapping("/{id}")
		public Caregiver getCaregiverById(@PathVariable(value = "id") String caregiverId) throws Exception {
		    return caregiverService.getCaregiverById(caregiverId);
		}
		
		// edit 
		@RequestMapping("/edit/{id}")
		public String showEditCaregiverPage(@PathVariable(name = "id") String id, Model model) {
		    Caregiver caregiver = caregiverService.getCaregiverById(id);
		    model.addAttribute("caregiver", caregiver);
		     
		    return "createCaregiver";
		}

	    // Delete a Caregiver
		@GetMapping("/delete/{id}")
		public String deleteCaregiver(Model model, @PathVariable(value = "id") String caregiverId) throws Exception {
			Caregiver caregiver = caregiverService.getCaregiverById(caregiverId);

		    caregiverService.delete(caregiver);

		    List<Caregiver> listCaregivers = caregiverService.findAll();
		    model.addAttribute("listCaregivers", listCaregivers);
		     
		    return "indexCaregiver";
		}
		
		@RequestMapping("/get")
		public String viewHomePage(Model model) {
		    List<Caregiver> listCaregivers = caregiverService.findAll();
		    model.addAttribute("listCaregivers", listCaregivers);
		     
		    return "indexCaregiver";
		}

}
