package com.example.medicalapp.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.medicalapp.model.Activity;
import com.example.medicalapp.service.RabbitMQSender;

@RestController
@RequestMapping(value = "/rabbit-app/")
public class RabbitMQWebController {

	@Autowired
	RabbitMQSender rabbitMQSender;

	@GetMapping(value = "/producer")
	public String producer() {
		
		File file = new File("C:\\Users\\Maria\\Documents\\workspace-sts-3.9.10.RELEASE\\medical-app\\src\\main\\resources\\static\\activity.txt"); 
		  try {
		  BufferedReader br = new BufferedReader(new FileReader(file)); 

		  String st; 
		  int patientId = 1;
		  int countMessages = 1;
		  while ((st = br.readLine()) != null) {
			  String info[] = st.split("        ");
		      System.out.println(st);
		      Activity msg=new Activity();
		  	
		  	  msg.setPatientId(patientId + "");
		  	  msg.setStartTime(info[0]);
		  	  msg.setEndTime(info[1]);
		  	  msg.setActivity(info[2]);
		  	  Thread.sleep(1000);
		      rabbitMQSender.send(msg);
		      
		      countMessages++;
		      
		      if (countMessages % 3 == 0) {
		    	  patientId ++;
		      }
		      } 
		  }catch (Exception e) {
			  e.printStackTrace();
		  }
		

		return "Messages sent to the RabbitMQ Successfully";
	}

}
