package com.example.medicalapp.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.api.activities.Activity;
import com.example.demo.api.activities.GetActivityRequest;
import com.example.demo.api.client.SoapClient;

@Controller
@RequestMapping("/activities")
public class ActivityController {

	@Autowired
	private SoapClient client;
	
	@Autowired
	private GetActivityRequest request;
	
	
	@RequestMapping("/get")
	public String invokeSoapClientToGetActivities(Model model) {
		request.setId(1);
		ArrayList<Activity> list = new ArrayList<>();
		list.addAll(client.getActivities(request).getData());
		model.addAttribute("listActivities", list);
		return "indexActivity";
	}
}
