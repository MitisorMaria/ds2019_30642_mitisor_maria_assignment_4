package com.example.medicalapp.consumer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer implements MessageListener{
	
	@Override
	
	@RabbitListener(queues = "${rabbitmq.queue}")
	public void onMessage(Message msg) {
		System.out.println("ReceivedMessage: " + msg);
		try {
			String message = msg.toString();
			int startIndex = message.indexOf("startTime") + 12;
			int endIndex = message.indexOf("endTime") + 10;
			int activityIndex = message.indexOf("activity") + 11;
			String date = (message.substring(startIndex, startIndex + 19));
			Date start = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(message.substring(startIndex, startIndex + 19));
			Date end = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(message.substring(endIndex, endIndex + 19));
			String activity = message.substring(activityIndex);
			activity = activity.substring (0, activity.length() - 3);
			long diff = start.getTime() - end.getTime();
			long diffHours = diff / (60 * 60 * 1000) % 24;
		
			System.out.println ("Start time:" + start + "End time: " + end + "Activity: " + activity + "diff: " + diffHours);
			
			if (diffHours > 12) {
				if (activity.equals("Sleeping"))
					System.out.println("Too much sleep");
				if (activity.equals("Leaving"))
					System.out.println("Too much outside time");
			}
			
			if ((diffHours > 1) && (activity.equals("Toileting")))
					System.out.println("Too much time on the toilet");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	
}
