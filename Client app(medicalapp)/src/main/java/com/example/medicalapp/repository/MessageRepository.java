package com.example.medicalapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.medicalapp.model.Activity;

@Repository
public interface MessageRepository extends JpaRepository<Activity, String>{

}
