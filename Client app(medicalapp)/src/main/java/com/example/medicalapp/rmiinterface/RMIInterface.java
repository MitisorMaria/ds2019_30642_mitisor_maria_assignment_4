package com.example.medicalapp.rmiinterface;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface RMIInterface extends Remote {
	public ArrayList<MedicationInterface> getMedPlan(int patId) throws RemoteException;
}