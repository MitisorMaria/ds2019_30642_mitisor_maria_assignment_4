package com.example.medicalapp.service;

import java.util.List;
import com.example.medicalapp.model.Patient;

public interface PatientService {
	Patient save(Patient patient);
    List<Patient> findAll();
    void delete(Patient p);

    Patient getPatientById(String id);

	Patient update(Patient patientDetails) throws Exception;
}
