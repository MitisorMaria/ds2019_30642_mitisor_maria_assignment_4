package com.example.medicalapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.medicalapp.model.Caregiver;
import com.example.medicalapp.repository.CaregiverRepository;

@Service
@Transactional
public class CaregiverServiceImpl implements CaregiverService{
	@Autowired
	private CaregiverRepository caregiverRepository;

	public List<Caregiver> findAll() {
		List<Caregiver> list = new ArrayList<>();
		caregiverRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(Caregiver p) {
		caregiverRepository.delete(p);
	}


	@Override
	public Caregiver getCaregiverById(String id) {
		Optional<Caregiver> optionalCaregiver = caregiverRepository.findById(id);
		return optionalCaregiver.isPresent() ? optionalCaregiver.get() : null;
	}

    @Override
    public Caregiver update(Caregiver caregiverDetails) throws Exception {
    	Caregiver updatedCaregiver = caregiverRepository.save(caregiverDetails);
	    return updatedCaregiver;
	    
	    
    }

    @Override
    public Caregiver save(Caregiver caregiver) {
        return caregiverRepository.save(caregiver);
    }

}
