package com.example.medicalapp.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.medicalapp.model.Activity;
import com.example.medicalapp.repository.MessageRepository;

@Service
public class RabbitMQSender {
	
	@Autowired
	private MessageRepository msgRepository;
	
	private static int msgId;
	
	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	
	@Value("${rabbitmq.exchange}")
	private String exchange;
	
	@Value("${rabbitmq.routingKey}")
	private String routingkey;	
	
	public void send(Activity msg) {
		rabbitTemplate.convertAndSend(exchange, routingkey, msg);
		msg.setId(msg.getId() + msgId++);
	    msgRepository.save(msg);
	}
}
