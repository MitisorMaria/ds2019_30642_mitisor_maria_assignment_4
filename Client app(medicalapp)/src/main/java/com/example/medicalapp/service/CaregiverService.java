package com.example.medicalapp.service;

import java.util.List;

import com.example.medicalapp.model.Caregiver;

public interface CaregiverService {
	Caregiver save(Caregiver caregiver);
    List<Caregiver> findAll();
    void delete(Caregiver p);

    Caregiver getCaregiverById(String id);

    Caregiver update(Caregiver caregiverDetails) throws Exception;
}
