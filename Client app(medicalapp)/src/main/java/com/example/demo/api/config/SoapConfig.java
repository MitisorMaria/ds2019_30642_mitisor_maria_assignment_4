package com.example.demo.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.example.demo.api.activities.GetActivityRequest;
import com.example.demo.api.client.SoapClient;

@Configuration
public class SoapConfig {
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller=new Jaxb2Marshaller();
		marshaller.setPackagesToScan("com.example.demo.api.activities");
		return marshaller;
	}
	
	@Bean
	public SoapClient client() {
		return new SoapClient();
	}
	
	@Bean
	public GetActivityRequest request() {
		return new GetActivityRequest();
	}

}
