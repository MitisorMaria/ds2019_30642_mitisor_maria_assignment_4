package com.example.demo.api.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.example.demo.api.activities.GetActivityRequest;
import com.example.demo.api.activities.GetActivityResponse;
import com.example.demo.api.activities.LISTTYPE;

@Service
public class SoapClient {
	@Autowired
	private Jaxb2Marshaller marshaller;

	private WebServiceTemplate template;

	public LISTTYPE getActivities(GetActivityRequest request) {
		template = new WebServiceTemplate(marshaller);
		GetActivityResponse acknowledgement = (GetActivityResponse) template.marshalSendAndReceive("http://localhost:8091/soapWS/",
				request);
		LISTTYPE l = acknowledgement.getActivity();
		return l;
	}

}
